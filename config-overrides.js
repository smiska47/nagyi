const { getBabelLoader, override, addExternalBabelPlugins, addWebpackPlugin, disableEsLint } = require('customize-cra')
const webpack = require('webpack')

const prependBabelPlugin = (plugin) => (config) => {
    getBabelLoader(config).options.plugins.unshift(plugin);
    return config;
};

// if (process.env.NODE_ENV !== 'production') {
//     analytics.disable();
//   }

//module.exports = override(prependBabelPlugin('@babel/plugin-proposal-class-properties'));
module.exports = override(
    ...addExternalBabelPlugins(
        ['@babel/plugin-proposal-class-properties', { loose: true }],
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-transform-regenerator',
        [
            '@babel/plugin-transform-runtime',
            {
                helpers: false,
            },
        ],
    ),
    addWebpackPlugin(new webpack.ContextReplacementPlugin(
        /\/encoding\//,
        (data) => {
            delete data.dependencies[0].critical;
            return data;
        },
    )),
    disableEsLint()
);