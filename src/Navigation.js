import React from 'react'
import {
    Link
} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faEnvelope, faComments } from '@fortawesome/free-solid-svg-icons'

library.add(faComments)
library.add(faHome)
library.add(faEnvelope)

const Navigation = () => {
    return (
        <nav>
            <Link to="/chat">
                <FontAwesomeIcon
                    icon="comments"
                />
            </Link>
            <Link to="/">
                <FontAwesomeIcon
                    icon="home"
                />
            </Link>
            <Link to="/groups">
                <FontAwesomeIcon
                    icon="envelope"
                />
            </Link>
        </nav>
    )
}


export default Navigation