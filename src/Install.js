import React from "react";

const InstallPWA = () => {

    //const [supportsPWA, setSupportsPWA] = useState(false);
    //const [promptInstall, setPromptInstall] = useState(null);

    const onClick = (e) => {
        // Hide the app provided install promotion
        //hideMyInstallPromotion();
        // Show the install prompt
        window.deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        window.deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the install prompt');
            } else {
                console.log('User dismissed the install prompt');
            }
        })
    };

    return (
        <>
            <p style={{ marginTop: "40px" }}>Kérlek kattints a gombra a Nagyi alkalmazás telepítéséhez:</p>
            <button
                className="link-button"
                id="add-button"
                aria-label="Install app"
                title="Install app"
                onClick={onClick}
                style={{
                    height: "10vh",
                    width: "50vw",
                    margin: "0 auto"
                }}
            >
                Install
        </button>
        </>
    );
};

export default InstallPWA;