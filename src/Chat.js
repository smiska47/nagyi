import React, { Component } from 'react'
// import {
//   //  Switch,
//   Route,
//   //  Link,
//   Redirect,
//   //useParams,
//   //useRouteMatch
// } from "react-router-dom";
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
} from "react-router-dom"
//import ChatService from '../services/chat-service'
import AuthService from './components/chat/services/auth-service'
import Auth from './components/chat/components/auth/auth'
import Home from './components/chat/components/home/home'
import Loader from './components/chat/helpers/loader/loader'

import store from './components/chat/store'

export default class Chat extends Component {
  constructor(props) {
    super(props)
    this.state = {
      routName: false,
      isLoader: true
    }
    this.initUser()
  }

  initUser = async () => {
    const routLink = await AuthService.init()
    this.setState({ routName: routLink, isLoader: false })
  }

  render() {
    const { routName, isLoader } = this.state
    console.log("2", routName, isLoader, this.props)

    return (
      <Router basename={`${process.env.PUBLIC_URL}/chat`}>
        <Provider store={store}>
          {isLoader ?
            <div style={{ position: 'absolute', width: '100%', height: '100%' }}>
              <Loader />
            </div>
            : <>
              <Route path={`/home`} component={Home} />
              <Route path={`/auth`} component={Auth} />
              <Redirect to={`${routName}`} />
            </>
          }
        </Provider>
      </Router>
    )
  }
}