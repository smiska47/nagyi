import React, { useState } from "react"
import Spinner from './components/PolygonSpinner'

export function Groups(params) {
    //const [isLoaded, setLoaded] = useState(false)
    const groupsIframeSrc = `https://groups.google.com/forum/embed/?place=forum/fekete-nagyi&showsearch=false&showpopout=false&showtabs=false&parenturl=${encodeURIComponent(window.location.href)};`
    console.log("groupsIframeSrc", groupsIframeSrc)
    useEffect(() => {
        setTimeout(() => setLoaded(true), 3000)
    }, [])

    return (
        isLoaded ? (
            <iframe id="forum_embed"
                src={groupsIframeSrc}
                title="Nagyi google groups"
                scrolling="no"
                frameBorder="0"
                height="100%"
                onLoad={() => {
                    console.log("groups iframe loaded")
                }} />
        ) : <Spinner className="image-loader-100p" />
    )
}

