import React from "react"
import AwesomeSlider from 'react-awesome-slider'
import withAutoplay from 'react-awesome-slider/dist/autoplay'
import 'react-awesome-slider/dist/custom-animations/cube-animation.css'

import CoreStyles from 'react-awesome-slider/src/core/styles.scss';
import AnimationStyles from 'react-awesome-slider/src/styled/fold-out-animation/fold-out-animation.scss';


import nagyi1 from "../assets/nagyi1.jpg"
import nagyi2 from "../assets/nagyi2.jpg"
import nagyi3 from "../assets/nagyi3.jpg"
import nagyi4 from "../assets/nagyi4.jpg"
import nagyi5 from "../assets/nagyi5.jpg"
import nagyi6 from "../assets/nagyi6.jpg"
import nagyi7 from "../assets/nagyi7.jpg"
import nagyi8 from "../assets/nagyi8.jpg"

//import 'react-awesome-slider/dist/styles.css'

const AutoplaySlider = withAutoplay(AwesomeSlider)
const images = () => {
    return [nagyi1, nagyi2, nagyi3, nagyi4, nagyi5, nagyi6, nagyi7, nagyi8].map(pathString => {
        return { source: pathString }
    })
}

export const Slider = () => (
    <AutoplaySlider
        className="awesomeSlider"
        animation="cubeAnimation"
        cssModule={[CoreStyles, AnimationStyles]}
        play={true}
        cancelOnInteraction={false} // should stop playing on user interaction
        interval={2000}
        media={images()}
        buttons={false}
        bullets={false}
    >
    </AutoplaySlider>
)