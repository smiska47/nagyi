import React from 'react'
import './splashPage.scss'

export default function SplashPage() {
  return (
    <div className="splash-page-container">
      <p>Please select a chat to start messaging</p>
    </div>
  )
}
