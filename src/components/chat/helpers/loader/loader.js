import React, { Component } from 'react'
import './loader.scss'

export default class Loader extends Component {
  render() {
    return (
      <div className="container-loader">
        <div className="loader"></div>
      </div>
    )
  }
}
