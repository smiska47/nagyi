import React from "react"

export const ApplyButton = ({ label, daypart, responsible, handleApply }) => {
    return (
        <div key={daypart} className={`indicatorContainer ${responsible ? 'taken' : ''}`} onClick={(e, daypart) => handleApply(e, daypart)}>
            {label}
            <div className='indicatorMsg'>
                {responsible ? `Lefoglalva - ${responsible} intézi` : `Keresi emberét`}
            </div>
        </div>
    )
}