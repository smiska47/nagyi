const config = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    clientId: process.env.REACT_APP_CLIENT_ID
}

class Calendar {
    constructor() {
        window.CLIENT_ID = config.clientId
        window.API_KEY = config.apiKey
    }

    static getCredentials() {
        return config
    }

    // create = ({ action, work, branch, createdAt }) => {
    //     this.db.collection("activities").add({
    //         action, work, branch, createdAt: createdAt
    //     }).then(({ id }) => {

    //         // this.currentDocId = id
    //         console.log("Document written with ID: ", id);
    //         this.currentDoc = id
    //     }).catch(function (error) {
    //         throw new Error(error)
    //         //console.error("Error adding document: ", error);
    //     });
    // }

    // readOne = () => {
    //     this.db.collection("activities").doc(this.currentDoc).get()
    //         .then(({ data }) => {
    //             //debugger
    //             console.log(data)

    //             return data
    //         })
    //         .catch(e => { throw new Error(e) })
    // }

    // updateOne = () => {
    //     this.db.collection("activities").doc(this.currentDoc).update({ finishedAt: Date.now() })
    //         .then(obj => console.log("update obj", obj))
    //         .catch(err => console.log("update err", err))
    // }

    // delete = () => {

    // }
}

export default Calendar