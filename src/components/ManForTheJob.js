import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ApplyButton } from './ApplyButton'
//import { arraysEqual } from '../utility'

const user = JSON.parse(localStorage.getItem("WhoAmI"))
console.log("localstorage usercreation", user)

export const ManForTheJob = () => {
    const [responsibles, setResponsibles] = useState({})
    //const [whoAmIFull, setWhoAmIFull] = useState('')
    const [whoAmINick, setWhoAmINick] = useState('')

    useEffect(() => {
        console.log("xxx", JSON.stringify(responsibles))
        // .readOne().then(data => {
        //     if (typeof data === 'undefined') {
        //         .create({ morning: null, noon: null, evening: null })
        //     } else {
        //         if (arraysEqual(responsibles, data)) {
        //             return
        //         }
        //         setResponsibles(data)
        //     }
        // })

    }, [responsibles])

    //useEffect(() => setWhoAmIFull(user.fullName), [])
    useEffect(() => user && setWhoAmINick(user.hopeFullyNick), []) // implement refresh notification to get user surely

    const createNotif = () => {
        if ('Notification' in window) {
            //debugger
            navigator.serviceWorker.ready.then(registration => {
                registration.showNotification('Vibration Sample', {
                    body: 'Buzz! Buzz!',
                    tag: 'vibration-sample'
                });
            });
        } else {
            alert("Szólj Misinek, nálad nincs Notification interfész :(")
        }
    }

    const handleSubmit = async (e, id) => {
        e.preventDefault()
        debugger
        setResponsibles({ [id]: whoAmINick });
        // .updateOne({ [id]: whoAmINick }).then(res => {
        //     debugger
        // })
    }

    const renderAwesome = () => (
        <FontAwesomeIcon
            icon='medal'
        />
    )


    const handleMockClick = (e) => {
        e.preventDefault();
        setResponsibles({ morning: "Petike", noon: "Marika", evening: "Lacika" });
    }


    return (
        <>
            <span className="briefingText">Kattints az idősávok valamelyikére hogy jelentkezz az adott eseményre</span>
            <ApplyButton label="Reggel" daypart="morning" responsible={responsibles.morning} handleApply={handleSubmit} />
            <ApplyButton label="Délben" daypart="noon" responsible={responsibles.noon} handleApply={handleSubmit} />
            <ApplyButton label="Este" daypart="evening" responsible={responsibles.evening} handleApply={handleSubmit} />

            <div className='indicatorMsg'>
                {Object.values(responsibles).every(o => o === null)
                    ? "Családi összefogás kell emberek!"
                    : (
                        <>
                            <span>
                                {`Gratulálunk - ${responsibles.morning}, ${responsibles.noon}, ${responsibles.evening} nagyon ügyesek vagytok!`}
                            </span>
                            <span>{renderAwesome()}</span>
                        </>
                    )}
            </div>
            <button onClick={handleMockClick}>Toggle</button>
            <button onClick={createNotif}>createNotif</button>
        </>
    )
}