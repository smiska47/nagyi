import "./PolygonSpinner.scss";
import React, { Component } from "react";
import classNames from "classnames";

// !! COPIED FROM React\Web\src\components\utility TODO: MOVE TO COMMON

export default class PolygonSpinner extends Component {
    render() {
        const { color } = this.props;
        const spinnerClass = classNames("poly-spinner", {
            "poly-spinner--red": color === "red",
        });

        return (
            <div className={classNames("poly-spinner__container", this.props.className)}>
                <div className={spinnerClass}>
                    <svg id="outline-slim" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 98.8 94">
                        <path fill="none" strokeWidth="1" strokeMiterlimit="10" d="M35 2.5h28.9l23.4 17L96.2 47l-8.9 27.5-23.4 17H35l-23.4-17-9-27.5 9-27.5z" />
                    </svg>
                    <svg id="outline-fat" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 98.8 94">
                        <path
                            fill="none"
                            strokeWidth="5"
                            strokeLinecap="round"
                            strokeMiterlimit="10"
                            d="M35 2.5h28.9l23.4 17L96.2 47l-8.9 27.5-23.4 17H35l-23.4-17-9-27.5 9-27.5z"
                        />
                    </svg>
                </div>
            </div>
        );
    }
}