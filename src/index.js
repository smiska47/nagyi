import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"

import { Groups } from './Groups'

import './index.scss'
import App from './App'
import Chat from './Chat'
import Navigation from './Navigation'
import { isSignedFor } from './utility'

// import { createStore } from 'redux'
// import { Provider } from 'react-redux'
// import rootReducer from './components/chat/reducers'

import * as serviceWorker from './serviceWorker'

const Index = props => {
    const [showReload, setShowReload] = useState(false);
    const [waitingWorker, setWaitingWorker] = useState(null);

    const reloadPage = () => {
        waitingWorker?.postMessage({ type: 'SKIP_WAITING' });
        setShowReload(false);
        window.location.reload(true);
    };

    const reloadModal = () => (
        <div className="reloadModal">
            <h2>Az alkalmazás frissült</h2>
            <p>Töltsd újra az oldalt a gombra kattintva</p>
            <button onClick={() => reloadPage()}>Újratölt</button>
        </div>
    )

    const onSWUpdate = registration => {
        setShowReload(true);
        setWaitingWorker(registration.waiting);
    };

    useEffect(() => {
        serviceWorker.register({ onUpdate: onSWUpdate });
    }, []);

    return (
        <>
            <Router basename={process.env.PUBLIC_URL}>
                {showReload && reloadModal()}
                <Switch className={`${showReload && 'bgBlur'}`}>
                    <Route exact path="/" render={props => <App {...props} isSignedFor={isSignedFor()} />} />
                    <Route path="/chat" component={Chat} />
                    <Route path="/groups" component={Groups} />
                </Switch>
                <div className="appMain">
                    <Navigation />
                </div>

            </Router>
        </>
    )
}


ReactDOM.render(<Index />,
    document.getElementById('root')
)
