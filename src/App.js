import React, { useEffect, useState } from 'react'
import { GoogleLogin } from 'react-google-login'
import A2HS1 from 'react-add-to-homescreen';
//import A2HS2 from '@ideasio/add-to-homescreen-react';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faMedal } from '@fortawesome/free-solid-svg-icons'

import './App.scss'
import { ManForTheJob } from './components/ManForTheJob'
import { Slider } from './components/Slider'
import InstallPWA from './Install'
import { gApiInit } from './utility'

library.add(faMedal)

//import BigCalendar from 'react-big-calendar'
// a localizer for BigCalendar
//BigCalendar.momentLocalizer(moment)


//let url = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events?key=${API_KEY}`

// this weird syntax is just a shorthand way of specifying loaders
//require('style-loader!css-loader!react-big-calendar/lib/css/react-big-calendar.css')

//       if (!err) {
//         const events = []
//         JSON.parse(resp.text).items.map((event) => {
//           events.push({
//             start: event.start.date || event.start.dateTime,
//             end: event.end.date || event.end.dateTime,
//             title: event.summary,
//           })
//         })
//         callback(events)
//       }
//     })
// }

const App = () => {
  const [signedForNagyi, setSignedForNagyi] = useState(0)
  const [rerender, setReRender] = useState(false)


  useEffect(() => {
    //console.log('useEffect 1')
    gApiInit(setSignedForNagyi)

    if (Notification.permission === 'granted') {
      localStorage.setItem('permissionGranted', true)
      setReRender(prev => !prev)
    }
  }, [])

  // window.addEventListener('storage', () => {
  //   if (Number(localStorage.getItem('signedForNagyi'))) {
  //     return
  //   } else {
  //     setSignedForNagyi(0)
  //   }
  // })

  const responseGoogle = (response) => {
    gApiInit(setSignedForNagyi)
    console.log("responseGoogle", response)
    window.gme = response
  }

  const resFailure = (a, b, c) => {
    console.log('something went wrong with google auth')
  }

  const addNotifs = () => {
    Notification.requestPermission().then(function (result) {
      if (result === 'granted') {
        localStorage.setItem('permissionGranted', true)
        setReRender(prev => !prev)
      }
    })
  }

  const handleAddToHomescreenClick = () => {
    alert(`
      1. Open Share menu
      2. Tap on "Add to Home Screen" button`);
  };

  //const toRender = this.state.events.map(e => <p>{e}</p>)


  return (
    <>
      {console.log('render')}
      {
        !signedForNagyi ? (
          <>
            <div className='appContainer'>
              <div>
                <GoogleLogin
                  clientId='29721733972-2q3bdrs3fd5cn4nctp70v9b87acaat72.apps.googleusercontent.com'
                  buttonText='Belépés a Nagyi appba'
                  onSuccess={responseGoogle}
                  onFailure={resFailure}
                  cookiePolicy={'single_host_origin'}
                />
              </div>
              <div className='sliderContainer'><Slider /></div>
            </div>


          </>

        ) : (
            <>
              <iframe
                title='Nagyi naptár'
                src='https://calendar.google.com/calendar/embed?height=200&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FBudapest&amp;src=dWtlbTNyN3RiZjNtYmV2YW1idXBvdWo0cmdAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23D50000&amp;showDate=1&amp;showPrint=0&amp;showCalendars=1&amp;showTabs=0&amp;mode=AGENDA&amp;hl=hu&amp;showTz=0&amp;showTitle=1'
                style={{ minHeight: 275 }} width='100%' height='auto' frameBorder='0' scrolling='no'>
              </iframe>
              {localStorage.getItem('permissionGranted') === 'true'
                ? <ManForTheJob />
                : (
                  <button onClick={addNotifs}> Kérlek kattints ide és engedélyezd az értesítéseket </button>
                )
              }
              <A2HS1 className="installbtn" onAddToHomescreenClick={handleAddToHomescreenClick} />
              {/* <A2HS2 className="installbtn" /> */}
              <InstallPWA className="installbtn" />
            </>
          )
      }
    </>
    // React Components in JSX look like HTML tags
    // <BigCalendar
    //   style={{ height: '420px' }}
    //   events={this.state.events}
    // />
  )
}

export default App