export const isSignedFor = () => {
    return Boolean(Number(localStorage.getItem('signedForNagyi')))
}

export const gApiInit = (setSignedForNagyi) => {
    window.gapi && window.gapi.load('auth2', async function () {
        try {
            const gApiAuthObj = await window.gapi.auth2.getAuthInstance({
                client_id: '29721733972-2q3bdrs3fd5cn4nctp70v9b87acaat72.apps.googleusercontent.com'
            })
            if (gApiAuthObj) {
                const user = gApiAuthObj.currentUser.ie && gApiAuthObj.currentUser.ie.Pt || {}
                console.log("API init ran", gApiAuthObj)
                window.gApiAuthObj = gApiAuthObj

                setSignedForNagyi(gApiAuthObj.isSignedIn.get() ? 1 : 0)

                user
                    ? localStorage.setItem("WhoAmI", JSON.stringify({ fullName: user.Ad, hopeFullyNick: user.BW }))
                    : localStorage.setItem("WhoAmI", JSON.stringify({ fullName: "", hopeFullyNick: "" }))

            }
        } catch (error) {
            throw new Error(error)
        }
    })
}

export const arraysEqual = (arr1, arr2) => {
    if (arr1.length !== arr2.length)
        return false;
    for (var i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i])
            return false;
    }

    return true;
}